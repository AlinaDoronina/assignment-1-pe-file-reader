var structPEHeader =
[
    [ "Characteristics", "structPEHeader.html#a5b823ed42dd5f0fc7b62203d170ea1eb", null ],
    [ "Machine", "structPEHeader.html#a24e289a20d9ed1becaa5d20a4fb265ca", null ],
    [ "Magic", "structPEHeader.html#adb1314576c83c9725f6a699831aad593", null ],
    [ "NumberOfSections", "structPEHeader.html#a2f30155c9a4c054d5c9030b7af2902d7", null ],
    [ "NumberOfSymbols", "structPEHeader.html#acc70d304f460fd41b68616c5581bfa17", null ],
    [ "PointerToSymbolTable", "structPEHeader.html#a4bd49937405075d9974939d40d118512", null ],
    [ "SizeOfOptionalHeader", "structPEHeader.html#a1d2ae31fd68935846b59777004b60ca2", null ],
    [ "TimeDataStamp", "structPEHeader.html#a954577e49a82d545f3b1c0810ccd8fed", null ]
];