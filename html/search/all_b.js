var searchData=
[
  ['open_5ferror_50',['OPEN_ERROR',['../IO__file_8c.html#a73aad7f7ea4c833e43281fb5b129b82fa10bfe36115b5906a85ed387f04fa6bdf',1,'IO_file.c']]],
  ['open_5ffile_51',['open_file',['../IO__file_8c.html#ace759cc514e10de95a50608a41fc195d',1,'IO_file.c']]],
  ['open_5ffile_5fhandler_52',['open_file_handler',['../IO__file_8h.html#a7dc4a17903a8e730eafbb13744321a7b',1,'open_file_handler(const char *path, FILE **file, enum fopen_argument fopen_argument_enum):&#160;IO_file.c'],['../IO__file_8c.html#a7dc4a17903a8e730eafbb13744321a7b',1,'open_file_handler(const char *path, FILE **file, enum fopen_argument fopen_argument_enum):&#160;IO_file.c']]],
  ['open_5fok_53',['OPEN_OK',['../IO__file_8c.html#a73aad7f7ea4c833e43281fb5b129b82fa430d4c7587b132b2d47db1f9b28fbcc2',1,'IO_file.c']]],
  ['open_5fstatus_54',['open_status',['../IO__file_8c.html#a73aad7f7ea4c833e43281fb5b129b82f',1,'IO_file.c']]],
  ['optional_5fheader_55',['optional_header',['../structPEFile.html#aaae3fc75b24b99363e5abaa81e505b39',1,'PEFile']]],
  ['optional_5fheader_5foffset_56',['optional_header_offset',['../structPEFile.html#a885cca07f9d2ee7491085c4ba2ee9d0a',1,'PEFile']]]
];
