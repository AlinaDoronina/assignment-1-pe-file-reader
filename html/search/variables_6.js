var searchData=
[
  ['machine_141',['Machine',['../structPEHeader.html#a24e289a20d9ed1becaa5d20a4fb265ca',1,'PEHeader']]],
  ['magic_142',['magic',['../structPEFile.html#a29a2ea72028ab586e281f3dd6d5cb2a4',1,'PEFile']]],
  ['magic_143',['Magic',['../structPEHeader.html#adb1314576c83c9725f6a699831aad593',1,'PEHeader::Magic()'],['../structPEOptionalHeader.html#aff531e0cdef614cb9f3be4ee8ae0827a',1,'PEOptionalHeader::Magic()']]],
  ['majorlinkerversion_144',['MajorLinkerVersion',['../structPEOptionalHeader.html#a16416f6759b1d5d73bd75b1df7ffa23f',1,'PEOptionalHeader']]],
  ['minorlinkerversion_145',['MinorLinkerVersion',['../structPEOptionalHeader.html#acd07ee2abfc0da4dea1170af9db74068',1,'PEOptionalHeader']]]
];
