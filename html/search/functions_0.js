var searchData=
[
  ['close_5ffile_119',['close_file',['../IO__file_8c.html#a2169c75b15ce3ecd96b5f9454cef3f64',1,'IO_file.c']]],
  ['close_5ffile_5fhandler_120',['close_file_handler',['../IO__file_8h.html#a4f7f84e7b3d8e207b25b65fad73abe21',1,'close_file_handler(FILE **file):&#160;IO_file.c'],['../IO__file_8c.html#a4f7f84e7b3d8e207b25b65fad73abe21',1,'close_file_handler(FILE **file):&#160;IO_file.c']]],
  ['copy_5fsection_5fto_5foutfile_121',['copy_section_to_outfile',['../PE__file__process_8h.html#a278f968344f151050aeb6b9721e1f804',1,'copy_section_to_outfile(FILE *in, FILE *out, struct PEFile PEFile, uint16_t j):&#160;PE_file_process.c'],['../PE__file__process_8c.html#a278f968344f151050aeb6b9721e1f804',1,'copy_section_to_outfile(FILE *in, FILE *out, struct PEFile PEFile, uint16_t j):&#160;PE_file_process.c']]]
];
