var structPEFile =
[
    [ "header", "structPEFile.html#ae6528ed31f4227b9173847a3c8033088", null ],
    [ "header_offset", "structPEFile.html#a8d76255b546755a566f3c387462b4420", null ],
    [ "magic", "structPEFile.html#a29a2ea72028ab586e281f3dd6d5cb2a4", null ],
    [ "optional_header", "structPEFile.html#aaae3fc75b24b99363e5abaa81e505b39", null ],
    [ "optional_header_offset", "structPEFile.html#a885cca07f9d2ee7491085c4ba2ee9d0a", null ],
    [ "section_header_offset", "structPEFile.html#a3d8ef617d4ac7133b3614d4ee78e916e", null ],
    [ "section_headers", "structPEFile.html#a2563ee8f9da210b01d243089eff2e366", null ]
];