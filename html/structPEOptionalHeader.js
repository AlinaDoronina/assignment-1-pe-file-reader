var structPEOptionalHeader =
[
    [ "AddressOfEntryPoint", "structPEOptionalHeader.html#a492781f6d5b4e14ad7c8e2dd269cc3d7", null ],
    [ "BaseOfCode", "structPEOptionalHeader.html#adaf62437cc32c8b59e1b2ac5d0433584", null ],
    [ "Magic", "structPEOptionalHeader.html#aff531e0cdef614cb9f3be4ee8ae0827a", null ],
    [ "MajorLinkerVersion", "structPEOptionalHeader.html#a16416f6759b1d5d73bd75b1df7ffa23f", null ],
    [ "MinorLinkerVersion", "structPEOptionalHeader.html#acd07ee2abfc0da4dea1170af9db74068", null ],
    [ "SizeOfCode", "structPEOptionalHeader.html#a813b83ca25168fbff25d5c03df5cd6d9", null ],
    [ "SizeOfInitializedData", "structPEOptionalHeader.html#a5f1cc6ad9c54eb5f6e8b19b5f18b5132", null ],
    [ "SizeOfUninitializedData", "structPEOptionalHeader.html#a0671a97c555e2e35128f0471fba7f219", null ]
];