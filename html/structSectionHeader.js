var structSectionHeader =
[
    [ "Characteristics", "structSectionHeader.html#a99ea3dab8420c8b4a4baee36297dd2d7", null ],
    [ "Name", "structSectionHeader.html#a87019485ab7d1ccc5810e5f8db0a20c8", null ],
    [ "NumberOfLinenumbers", "structSectionHeader.html#a49e56a51203755bb74b83995ce128d00", null ],
    [ "NumberOfRelocations", "structSectionHeader.html#a881e9fc8c95e422b938ff5e61c81b76a", null ],
    [ "PointerToLinenumbers", "structSectionHeader.html#a75714b9b94dfcb3f8b3542d19fbd6e9e", null ],
    [ "PointerToRawData", "structSectionHeader.html#aba324a86a9ce68b36b0362755d924fb7", null ],
    [ "PointerToRelocations", "structSectionHeader.html#aa6c623817ba43c030fd4b08f6e458881", null ],
    [ "SizeOfRawData", "structSectionHeader.html#a9f42abe9bf601ab29b2510e457863788", null ],
    [ "VirtualAddress", "structSectionHeader.html#aaa06e91a66b52ed17240713132347390", null ],
    [ "VirtualSize", "structSectionHeader.html#a58e26588631e7030dbdd8a35cf2b8eb2", null ]
];