/// @file
/// @brief File with information about PE file structure: PE header, optional header and section header


#ifndef PE_FILE_H
#define PE_FILE_H
#include <stdint.h>


#if defined _MSC_VER
    #pragma pack(push,1)
#endif
/// @brief Struct of PE header of the .exe file
struct
#if defined __GNUC__
    __attribute__((packed))
 #endif
PEHeader {
    uint8_t  Magic[4];
    uint16_t Machine;
    uint16_t NumberOfSections;
    uint32_t TimeDataStamp;
    uint32_t PointerToSymbolTable;
    uint32_t NumberOfSymbols;
    uint16_t SizeOfOptionalHeader;
    uint16_t Characteristics;
};
#if defined _MSC_VER
    #pragma pack(pop)
#endif


#if defined _MSC_VER
    #pragma pack(push,1)
#endif
/// @brief Struct of PE optional header of the .exe file
struct
#if defined __GNUC__
    __attribute__((packed))
 #endif
PEOptionalHeader {
    uint16_t Magic;
    uint8_t  MajorLinkerVersion;
    uint8_t  MinorLinkerVersion;
    uint32_t SizeOfCode;
    uint32_t SizeOfInitializedData;
    uint32_t SizeOfUninitializedData;
    uint32_t AddressOfEntryPoint;
    uint32_t BaseOfCode;

};
#if defined _MSC_VER
    #pragma pack(pop)
#endif


#if defined _MSC_VER
    #pragma pack(push,1)
#endif
/// @brief Struct of section of the .exe file
struct
#if defined __GNUC__
    __attribute__((packed))
 #endif
SectionHeader {
    char     Name[8];
    uint32_t VirtualSize;
    uint32_t VirtualAddress;
    uint32_t SizeOfRawData;
    uint32_t PointerToRawData;
    uint32_t PointerToRelocations;
    uint32_t PointerToLinenumbers;
    uint16_t NumberOfRelocations;
    uint16_t NumberOfLinenumbers;
    uint32_t Characteristics;
};
#if defined _MSC_VER
    #pragma pack(pop)
#endif


#if defined _MSC_VER
    #pragma pack(push,1)
#endif
/// @brief Struct of PE file of the .exe file
struct
#if defined __GNUC__
    __attribute__((packed))
 #endif
PEFile
{
  uint32_t header_offset;
  uint32_t optional_header_offset;
  uint32_t section_header_offset;
  uint32_t magic;

  struct PEHeader header;
  struct PEOptionalHeader optional_header;
  struct SectionHeader* section_headers;
};
#if defined _MSC_VER
    #pragma pack(pop)
#endif


#endif
