/// @file
/// @brief File with io-process functions with files


#ifndef IO_H
#define IO_H

#include <stdbool.h>
#include <stdio.h>

    /// @brief Permissions for the open file
    enum fopen_argument  {
        RT,
        WT
    };

    bool open_file_handler (const char* path, FILE** file, enum fopen_argument fopen_argument_enum);
    bool close_file_handler (FILE** file);

#endif
