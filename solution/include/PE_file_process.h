/// @file
/// @brief File with functions with .exe files


#ifndef PE_FILE_ACTIVE_H
#define PE_FILE_ACTIVE_H

#include <stdint.h>
#include <stdio.h>

#include "../include/PE_file_structure.h"

void read_PE_Header(struct PEFile* PEFile, FILE* file);
void read_optional_header(struct PEFile* PEFile, FILE* file);

uint16_t find_section(struct PEFile* PEFile, const char* section_name, FILE* file);
void initialize_section_header(struct PEFile* PEFile);
void free_section_header(struct PEFile* PEFile);

void copy_section_to_outfile(FILE* in, FILE* out, struct PEFile PEFile, uint16_t j);

#endif
