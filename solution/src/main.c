/// @file
/// @brief Main file

#include <stdint.h>
#include <stdio.h>

#include "../include/PE_file_structure.h"
#include "../include/IO_file.h"
#include "../include/PE_file_process.h"


int main(int argcnt, char** files) {

    if (argcnt < 3) return 1;

    /// Pointer to .exe file
    FILE* in = NULL;
    /// Path of the .exe file
    const char* path_in = files[1];
    /// Name of the desired section
    const char* section_name = files[2];
    if (!open_file_handler(path_in, &in, RT)) return 1;

    struct PEFile PEFile;
    read_PE_Header(&PEFile, in);
    read_optional_header(&PEFile, in);
    initialize_section_header(&PEFile);
    ///Number of the desired section
    const uint8_t number_of_section = find_section(&PEFile, section_name, in);

    /// Pointer to .bin file
    FILE* out = 0;
    /// Path of the .bin file
    const char* path_out = files[3];
    if (!open_file_handler(path_out, &out, WT)) return 1;
    copy_section_to_outfile(in, out, PEFile, number_of_section);

    if (!close_file_handler(&out) ||
        !close_file_handler(&in)
    ) return 1;

    free_section_header(&PEFile);

    return 0;

}
