/// @file
/// @brief File for io-process with files


#include <errno.h>
#include <inttypes.h> 
#include <stdbool.h>
#include  <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include "../include/IO_file.h"


/// @brief Array-translator permissions for the open file to its char* version
const char* const fopen_argiment_char[] = {
    [RT] = "rt",
    [WT] = "wt"
};


/// @brief File open status
enum open_status {
    OPEN_OK  = 0,
    OPEN_ERROR,
};


/// @brief File close status
enum close_status {
    CLOSE_OK = 0,
    CLOSE_ERROR
};


/// @brief Static function that open file at the given path
static enum open_status open_file (const char* path, FILE** file, enum fopen_argument fopen_argument_enum) {

  *file = fopen(path, fopen_argiment_char[fopen_argument_enum]);
  if (*file==NULL) return OPEN_ERROR;

  return OPEN_OK;
}


/// @brief Static function that close the file
static enum close_status close_file (FILE** file) {
  const int result_fclose = fclose(*file);
  if (result_fclose) return CLOSE_ERROR;
  return CLOSE_OK;
}


/// @brief Function that open file at the given path
/// @param path Path to the file wanted to open
/// @param file Double pointer to save the result of opening the file
/// @param  fopen_argument_enum Permissions for the open file
/// @return True: file opened without any errors
/// @return False: file opened with some errors
bool open_file_handler (const char* path, FILE** file, enum fopen_argument fopen_argument_enum) {

  const enum open_status result_open_file_read = open_file(path, file, fopen_argument_enum);
  if (result_open_file_read) {
    fprintf(stderr, "File opening error. Error code: %d\n", result_open_file_read);
    return false;
  }
  return true;
}


/// @brief Function that close the file
/// @param file Pointer to the file wanted to close
/// @return True: file closed without any errors
/// @return False: file closed with some errors
bool close_file_handler (FILE** file) {

  const enum close_status result_close_file = close_file(file);
  if (result_close_file) {
    fprintf(stderr, "File closing error. Error code: %d\n", result_close_file);
    return false;
  }
  return true;
}

