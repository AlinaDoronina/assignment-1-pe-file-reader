/// @file
/// @brief File for process with .exe files


#include "string.h"
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include "../include/PE_file_structure.h"


/// @brief Function that read the PE header to a PE file struct
/// @param PEFile Pointer to a PE file where to save PE header
/// @param in Pointer to a file to read information from
/// @return Nothing
void read_PE_Header(struct PEFile* PEFile, FILE* in) {
    
    /// Offset from the beginning of the file to the cell with information about the header offset
    const uint8_t MAIN_OFFSET = 0x3C;
    /// Size of the header offset field
    const uint8_t SIZE_PE_HEADER_OFFSET = 4;
    
    fseek(in, MAIN_OFFSET, SEEK_SET);
    fread(&(PEFile->header_offset), SIZE_PE_HEADER_OFFSET, 1, in);

    fseek(in, PEFile->header_offset, SEEK_SET);
    fread(&(PEFile->header), sizeof(struct PEHeader), 1, in);


}


/// @brief Function that read the optional header to a PE file struct
/// @param PEFile Pointer to a PE file where to save optional header
/// @param in Pointer to a file to read information from
/// @return Nothing
void read_optional_header(struct PEFile* PEFile, FILE* in) {

    PEFile->optional_header_offset = PEFile->header_offset+sizeof(struct PEHeader);
    fseek(in, PEFile->optional_header_offset, SEEK_SET);
    fread(&(PEFile->optional_header), sizeof(struct PEOptionalHeader), 1, in);

}


/// @brief Function that find the number of desired section
/// @param PEFile Pointer to a PE file with an array of sections' headers
/// @param section_name Name of the desired section
/// @param in Pointer to a file to read information from
/// @return Number of desired section
uint16_t find_section(struct PEFile* PEFile, const char* section_name, FILE* in) {

    uint16_t i = 0;
    PEFile->section_header_offset = PEFile->header_offset+sizeof(struct PEHeader)+PEFile->header.SizeOfOptionalHeader;
    fseek(in, PEFile->section_header_offset, SEEK_SET);

    while (i<PEFile->header.NumberOfSections) {
        fread(PEFile->section_headers+i, sizeof(struct SectionHeader), 1, in);
        if (!strcmp((PEFile->section_headers+i)->Name, section_name)) break;
        ++i;
    }

    return i;
}


/// @brief Function that initializes sections header
/// @param PEFile Pointer to a PE file with an array of sections' headers
/// @return Nothing
void initialize_section_header(struct PEFile* PEFile) {
    PEFile->section_headers = malloc(sizeof(struct SectionHeader)*PEFile->header.NumberOfSections);
}


/// @brief Function that free an array of sections' headers of the PE file
/// @param PEFile Pointer to a PE file with an array of sections' headers
/// @return Nothing
void free_section_header(struct PEFile* PEFile) {
    free(PEFile->section_headers);
}


/// @brief Function that free an array of sections' headers of PE file
/// @param PEFile Pointer to a PE file with an array of sections' headers
/// @return Nothing
void copy_section_to_outfile(FILE* in, FILE* out, struct PEFile PEFile, uint16_t j) {
    uint8_t* data = calloc((PEFile.section_headers+j)->SizeOfRawData, 1);

    fseek(in, (PEFile.section_headers+j)->PointerToRawData, SEEK_SET);
    fread(data, (PEFile.section_headers+j)->SizeOfRawData, 1, in);
    fwrite(data, (PEFile.section_headers+j)->SizeOfRawData, 1, out);

    free(data);
}
